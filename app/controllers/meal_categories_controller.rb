class MealCategoriesController < ApplicationController

    def index
        @meal_category = MealCategory.all 
        render json: @meal_category, status: 200
    end 

    def show
        @meal_category = MealCategory.find(params[:id])
        render json: @meal_category, status: 200
    end

    def create
        @meal_category = MealCategory.new(name: params[:name])
        if @meal_category.save
            render json: @meal_category, status: 200
        else
            render json: @meal_category.errors
        end
    end

    def update
        @meal_category = MealCategory.find(params[:id])
        @meal_category = @meal_category.update(name: params[:name])
        render json: @category, status: 200
    end 

    def delete
        @category = MealCategory.find(params[:id])
        @category.delete
    end


end
