class OrdersController < ApplicationController

    def index
        @orders = Order.all 
        render json: @orders, status: 200
    end

    def show
        @orders = Order.find(params[:id])
        render json: @orders, status: 200
    end

    def create
        @orders = Order.new(price: params[:price], situation_id: params[:idsituation])
        if @orders.save
            render json: @orders, status: 200
        else
            render json: @orders.errors
        end
    end

    def update
        @orders = Order.find(params[:id])
        @orders = @orders.update(price: 00, situation_id: params[:idsituation]) #colocar as ids#
        render json: @orders, status: 200
    end

    def delete
        @orders = Order.find(params[:id])
        @orders.delete
    end

end
