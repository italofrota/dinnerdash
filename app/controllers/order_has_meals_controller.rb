class OrderHasMealsController < ApplicationController
    def create
        @order_has_meal = OrderHasMeal.new(quantity: params[:quantity], meal_id: params[:meal_id])
        if @order_has_meal.save
            render json: @order_has_meal, status: 200
        else
            render json: @order_has_meal.errors
        end
    end

    def update
        @order_has_meal = OrderHasMeal.find(params[:id])
        @order_has_meal = @order_has_meal.update(quantity: params[:quantity], order_id: params[:order_id], meal_id: params[:meal_id])
        render json: @order_has_meal, status: 200
    end

    def delete
        @order_has_meal = OrderHasMeal.find(params[:id])
        @order_has_meal.delete
    end
end
