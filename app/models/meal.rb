class Meal < ApplicationRecord
#RELAÇÕES#
    belongs_to :meal_category
    has_many :order_has_meals
    before_update :isavailable

#VALIDAÇÕES#
    validates :name, presence: true
    validates :description, presence: true
    validates :price, presence: true
    validates :available, presence: true

    validates :name, length: { maximum: 45 }
    validates :description, length: { maximum: 45}
    # validates :price,  format: { with: /\A\d+(?:\.\d{2})?\z/ }, numericality: { greater_than: 0, less_than: 1000000 }
    validates :available, length: { maximum: 45 }

    def isavailable
        self.order_has_meals.each do |dish|
            if(dish.meal.available == "Indisponível")
                render json: { error: "Comida indisponivel" }
            else 
                render json: dish
            end
        end
    end

end
