class Order < ApplicationRecord
    has_many :order_has_meals
    belongs_to :situation
    before_update :setprice 

    def setprice
        @price = 0
        self.order_has_meals.each do |ordermeal|
            @price = (ordermeal.quantity * ordermeal.meal.price) + @price 
        end
        self.price = @price
    end

    validates :price, presence: true

end
