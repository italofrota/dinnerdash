class MealCategory < ApplicationRecord
    has_many :meals
    validates :name, presence: true
    validates :name, uniqueness: true
    validates :name, length: { maximum: 45 }

end
