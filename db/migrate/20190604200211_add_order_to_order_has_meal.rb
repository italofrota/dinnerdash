class AddOrderToOrderHasMeal < ActiveRecord::Migration[5.2]
  def change
    add_reference :order_has_meals, :order, foreign_key: true
  end
end
